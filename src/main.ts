import 'dotenv/config';
import "reflect-metadata";
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { createAppDataSource } from './data-sources';
import { Burger } from './entities/burgers.entity';
import { serverConfig } from './config';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

async function startServer() {
  try {
    const appDataSource = createAppDataSource();

    await appDataSource.initialize();

    app.get('/burgers', async (req, res) => {
      try {
        const burgers = await appDataSource.manager.find(Burger);
        res.json(burgers);
      } catch (error) {
        console.error('Error fetching burgers:', error);
        res.status(500).json({ error: 'Internal Server Error' });
      }
    });

    app.listen(serverConfig.port, () => {
      console.log(`Server is running on http://localhost:${serverConfig.port}`);
    });
  } catch (error) {
    console.error('Error setting up the database:', error);
    process.exit(1);
  }
}

startServer();
