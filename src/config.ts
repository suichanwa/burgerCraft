export const database = {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT || '5432'),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    synchronize: true,
    logging: true,
    entities: [__dirname + '/entities/**/*.ts'],
    migrations: [],
    subscribers: [],
};

export const serverConfig = { port: parseInt(process.env.PORT || '3000', 10) };