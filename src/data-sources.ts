import { DataSource, DataSourceOptions } from 'typeorm';
import { database } from './config';

export function createAppDataSource(): DataSource {
  return new DataSource({
    type: database.type,
    host: database.host,
    port: database.port,
    username: database.username,
    password: database.password,
    database: database.database,
    synchronize: true,
    logging: true,
    entities: database.entities,
  } as DataSourceOptions);
}